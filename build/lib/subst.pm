-- vim: set ft=lua sw=3 et:

SED = "sed '{%SEDSCRIPT%}' %in[1]% > %out[1]%"
SEDSCRIPT = EMPTY

SUBSTDICT = EMPTY

subst = simple {
   class = "subst",
   outputs = {"%U%-%I%.subst"},
   command = {
      "%SED%"
   },

   __init = function(self, p)
      simple.__init(self, p)

      if ((type(p) == "table") and p.class) then
         return
      end

      local dict = self:__index("dict")
      if not dict then
         dict = SUBSTDICT
      end

      if type(dict) ~= "table" then
         self:__error("type of dict should be table instead of ", type(dict))
      end
          

      self.SEDSCRIPT = ""

      for k, v in pairs(dict) do
         self.SEDSCRIPT = self.SEDSCRIPT .. "s/" .. k .. "/" .. v .. "/g;"
      end

   end
}

subst_in = subst {
   outputs = {"%{return string.gsub(self:__index('in')[1], '^(.*)\.in$', '%1')}%"},
}

