-- vim: set ft=lua sw=3 et:

POSIX_INSTALL = "cp -rd"

function posix.basename(f)
   local bn, n = string.gsub(f, "^.-([^/]*)$", "%1")
   return bn
end

posix_install = node {
   class = "posix_install",
   target = nil,
   ensure_at_least_one_child = true,
   construct_sting_children_with = file,
   target_is_dir = false,

   __init = function(self, p)
      node.__init(self, p)

      -- If we're a class, don't verify.

      if (type(p) == "table") and p.class then
         return
      end
         
      if not self.target then
         self:__error("no target specified for install command")
      end

      if type(self.target) ~= "string" then
         self:__error("install target should be a string instead of ", type(self.target))
      end
      
      if not self.target_is_dir and posix.stat(self.target, "type") == "directory" then
         self.target_is_dir = true
      end
   end,

   
   __outputs = function(self, inputs)
      local o = {}
         
      if self.target_is_dir or table.getn(inputs) ~= 1 then
         local target = self.target
         if string.sub(target, -1) ~= '/' then
            target = target .. '/' 
         end

         for k, v in ipairs(inputs) do
            table.insert(o, self:__expand(target .. posix.basename(self:__expand(v))))
         end
      else
         table.insert(o, self:__expand(self.target))
      end

      return o
   end,


   -- Building causes the 'install' application to be executed.
   __dobuild = function(self, inputs, outputs)
      if type(inputs) ~= "table" then
         inputs = {inputs}
      end

      local full_install_command = "%POSIX_INSTALL% "

      for _, v in ipairs(inputs) do
         full_install_command = full_install_command .. '"' .. v .. '"' .. " "
      end

      full_install_command = full_install_command .. '"' .. self.target .. '"'
      local r = self:__invoke(full_install_command)
      if r then
         self:__error("failed to install with return code ", r)
      end
   end,

   -- right now, cleaning is a no-op, maybe it should be like uninstall
   __doclean = function(self, inputs, outputs)
   end,

}
