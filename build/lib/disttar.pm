-- vim: set ft=lua sw=3 et:

TAR = "tar"

disttar = node {
   class = "disttar",

   basename = nil,
   version = nil,
   output = nil,
   exclude = {},

   ensure_at_least_one_child = true,
   construct_sting_children_with = file,

   __init = function(self, p)
      node.__init(self, p)

      -- If we're a class, don't verify.

      if (type(p) == "table") and p.class then
         return
      end
         
      if not self.basename then
         self:__error("no basename specified for disttar command")
      end
      
      if not self.version then
         self:__error("no version specified for disttar command")
      end
      
      if not self.output then
         self:__error("no output file specified for disttar command")
      end
   end,

   
   __outputs = function(self, inputs)
      local outputs = {}

      table.insert(outputs, self:__expand(self.output))

      return outputs
   end,


   -- Building causes a distribution tar file to be built
   __dobuild = function(self, inputs, outputs)
      local basename_version = self.basename .. "-" .. self.version
      
      local old_invoke = self.__invoke

      self.__invoke = function(self, command)
         local r = old_invoke(self, command)
         if r then
            self.__invoke = old_invoke
            self:__invoke("rm -rf " .. basename_version .. "/")
            self:__error("failed to execute '",  command, "' with return code ", r)
         end
      end

      if type(inputs) ~= "table" then
         inputs = {inputs}
      end

      local excludes = ""
      for _, v in pairs(self.exclude) do
         excludes = excludes .. " --exclude=" .. self:__expand(v)
      end

      self:__invoke("mkdir -p " .. basename_version .. "/")
      self:__invoke("cp -r " .. self:__expand("%in%") .. " " .. basename_version .. "/")
      self:__invoke("mkdir -p " .. posix.dirname(self.output))
      self:__invoke("tar -czf " .. self.output .. " " .. excludes .. " " .. basename_version .. "/")
      self:__invoke("rm -rf " .. basename_version .. "/")

      self.__invoke = old_invoke
   end,

   __doclean = function(self, inputs, outputs)
      self:__invoke({"%RM% %out%"})
      self:__invoke("rmdir " .. posix.dirname(self.output))
   end,

}
