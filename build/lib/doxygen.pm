-- vim: set ft=lua sw=3 et:

DOXYGEN = "doxygen"

function parse_doxyfile(filename) 
   local f = io.open(filename)
   if not f then
      return nil
   end

   local data = {}
   local current_key = nil
   local current_value = nil

   function whitespace()
      local c = f:read(1)
      while c do
         if c ~= " "
            and c ~= "\n" 
            and c ~= "\r"
            and c ~= "\t"
            and c ~= "\v"
         then
            return -1
         end
         c = f:read(1)
      end
      return 0
   end

   function to_eol()
      local c = f:read(1)
      while c ~= "\n" and c ~= nil do
         c = f:read(1)
      end
      return 0
   end

   function parse(func)
      local result = func()
      f:seek("cur", result)
      return result
   end

   function comment()
      local start = f:seek()
      local c = f:read(1)
      local comment = ""

      if not c then
         return 0
      end

      -- make sure c == '#' then read until the end of the line
      if c == "#" then
         c = f:read(1)
         while c ~= "\n" do
            comment = comment .. c
            c = f:read(1)
         end
      else
         return -1
      end
      return 0
   end
      
   function key()
      local c = f:read(1)
      if c == "#" then
         return -1
      end

      if not c then
         return 0
      end

      local keyword = ""
      while c ~= " " and  c~= "=" and c ~= nil do
         keyword = keyword .. c
         c = f:read(1)
      end

      current_key = keyword
      current_value = {}

      if c == "=" then
         f:seek("cur", -1)
      end
      return 0
   end

   function value_list()
      while 1 do
         parse(whitespace)
         parse(value)
         parse(whitespace)
         local c = f:read(1)
         if c == "\\" then
            c = f:read(1)
            if c ~= "\n" then
               return -2
            end
         else
            return -1
         end
      end
   end

   function value()
      local quoted = false
      local val = ""
      local c = f:read(1)

      if c == "\"" then
         quoted = true
         c = f:read(1)
      end

      while c ~= nil do
         if not quoted and (c == " " or c == "\n" or c == "\t") then
            break
         elseif quoted and (c == "\"") then
            break
         end

         val = val .. c
         c = f:read(1)
      end

      table.insert(current_value, val)
      return -1
   end


   function key_value_pair()
      local c = f:read(1)
      if c == "#" then
         return -1
      end
      f:seek("cur", -1)
      if parse(key) == 0 and f:read(0) then

         local c = f:read(1)
         while c ~= "=" and c ~= nil do
            c = f:read(1)
         end
         parse(value_list)
         data[current_key] = current_value
      end
      return 0
   end

   function root()
      while f:read(0) ~= nil do
         parse(whitespace)

         while parse(comment) == 0 and f:read(0) do end

         parse(whitespace)

         parse(key_value_pair)
      end
   end

   -- parse the file
   root()
   return data
end

doxyfile = node {
   class = "doxyfile",
   ensure_n_children = 1,
   construct_sting_children_with = file,

   __init = function(self, p)
      node.__init(self, p)

      -- If we're a class, don't verify.

      if (type(p) == "table") and p.class then
         return
      end

   end,
   
   __outputs = function(self, inputs)
      self.data = parse_doxyfile(inputs[1])
      if not self.data then
         self:__error("error parsing file '", inputs[1], "'")
      end
         
      return self.data["OUTPUT_DIRECTORY"]
   end,

   __dependencies = function(self, inputs, outputs)
      return self.data["INPUT"]
   end,

   -- Building causes the 'install' application to be executed.
   __dobuild = function(self, inputs, outputs)
      local r = self:__invoke("%DOXYGEN% %in[1]%")
      if r then
         self:__error("failed to execute doxygen with return code ", r)
      end
   end,

   -- right now, cleaning is a no-op, maybe it should be like uninstall
   __doclean = function(self, inputs, outputs)
      for _, i in ipairs(outputs) do
         self:__invoke("rm -rf " .. i)
      end
   end,

}
