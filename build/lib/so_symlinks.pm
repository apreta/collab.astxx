-- vim: set ft=lua sw=3 et:

so_symlinks = simple {
   outputs = {
      "%{return string.gsub(self:__index('in')[1], '^(.*\.so).*$', '%1.%%SO_VERSION%%')}%",
      "%{return string.gsub(self:__index('in')[1], '^(.*\.so).*$', '%1')}%",
   },
   command = {
      "ln -sf %in[1]% %out[1]% && ln -sf %in[1]% %out[2]%",
   }
}

