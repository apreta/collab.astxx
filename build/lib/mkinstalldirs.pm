-- vim: set ft=lua sw=3 et:

raw_string = node {
   class = "raw_string",
   ensure_n_children = 1,

   __build = function(self)
      if type(self[1]) ~= "string" then
         self:__error("type must be string instead of ", type(v))
      end
      return self:__expand(self[1])
   end,

   -- Outputs are inputs.

   __outputs = function(self, inputs)
      return inputs
   end,

   -- Building does nothing.

   __dobuild = function(self, inputs, outputs)
   end,

   -- Cleaning does nothing.

   __doclean = function(self, inputs, outputs)
   end,
}

mkinstalldirs = node {
   class = "mkinstalldirs",
   construct_string_children_with = raw_string,
   ensure_at_least_one_child = true,
   
   -- Outputs are inputs.

   __outputs = function(self, inputs)
      return inputs
   end,

   __dobuild = function(self, inputs, outputs)
      local r = self:__invoke("install -d %out%")
      if r then
         self:__error("failed to make directories with return code ", r)
      end
   end,

   -- Cleaning does nothing.

   __doclean = function(self, inputs, outputs)
   end,
}

