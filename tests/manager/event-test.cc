/* vim: set et sw=3 tw=0 fo=croqlaw cino=t0:
 *
 * Astxx, the Asterisk C++ API and Utility Library.
 * Copyright (C) 2005-2007  Matthew A. Nicholson
 * Copyright (C) 2005-2007  Digium, Inc.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <astxx/manager.h>
#include <vector>
#include <string>
#include <iostream>

void print_event(astxx::manager::message::event e) {
   std::cout << e.format();
}

int main(int argc, char** argv) {
   std::vector<std::string> args(argv, argv + argc);

   if (args.size() != 4) {
      std::cerr << "Usage: " << args[0] << " [host] [username] [secret]\n";
      return 1;
   }

   try {
      namespace manager = astxx::manager;
      namespace action = astxx::manager::action;

      manager::connection connection(args[1]);
      std::cout << "Connected to " << connection.name() << " v" << connection.version() << std::endl;

      boost::signals::scoped_connection c = connection.register_event("", print_event);
      action::login login(args[2], args[3]);
      login(connection);

      for (;;) {
         connection.wait_event();
         connection.pump_messages();
         connection.process_events();
      }
      return 0;
   }
   catch (const std::exception& e) {
      std::cerr << "Exception: " << e.what() << std::endl;
   }
   return 1;
}

