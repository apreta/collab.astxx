/* vim: set et sw=3 tw=0 fo=croqlaw cino=t0:
 *
 * Astxx, the Asterisk C++ API and Utility Library.
 * Copyright (C) 2005-2007  Matthew A. Nicholson
 * Copyright (C) 2005-2007  Digium, Inc.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <astxx/manager.h>
#include <vector>
#include <string>
#include <iostream>

int main(int argc, char** argv) {
   std::vector<std::string> args(argv, argv + argc);

   if (args.size() != 2) {
      std::cerr << args[0] << " takes a hostname as an argument\n";
      return 1;
   }

   try {
      astxx::manager::connection connection(args[1]);
      std::cout << "Connected to " << connection.name() << " v" << connection.version() << "\n";

      astxx::manager::message::response response = connection.send_action(astxx::manager::action::logoff());
      std::cout << connection.name() << " responsed '" << response["Message"] << "' to Logoff action.\n";
   }
   catch (const std::exception& e) {
      std::cerr << "Error: " << e.what() << std::endl;
      return 1;
   }
   return 0;
}
