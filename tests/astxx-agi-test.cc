/* vim: set et sw=3 tw=0 fo=croqlaw cino=t0:
 *
 * Astxx, the Asterisk C++ API and Utility Library.
 * Copyright (C) 2005-2007  Matthew A. Nicholson
 * Copyright (C) 2005-2007  Digium, Inc.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <map>
#include <astxx/agi.h>
#include <sstream>

using namespace std;
using namespace astxx;

int main(int argc, char** argv)
{
   try {
      astxx::agi& agi = astxx::agi::instance();

      agi.verbose("Welcome to the astxx::AGI test\n");
      agi.verbose("Environment is:", 2);
      for (map<string, string>::const_iterator i = agi.begin(); i != agi.end(); i++)
         agi.verbose(i->first + ": " + i->second, 2);

      agi.answer();

      stringstream ss;
      ss << "Channel status is " << agi.channel_status();
      agi.verbose(ss.str());

      ss.str("");

      ss << "Channel status is " << astxx::agi::instance().channel_status("Zap/1-1");
      agi.verbose(ss.str());
      agi.verbose("This concludes the astxx::agi test.");
   }
   catch (astxx::agi::error &e) {
      stringstream ss;
      ss << "An error occured in the agi script:\n   " << e.what();
      astxx::agi::instance().verbose(ss.str());
   }

   return 0;
}

