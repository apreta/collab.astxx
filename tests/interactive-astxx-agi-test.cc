/* vim: set et sw=3 tw=0 fo=croqlaw cino=t0:
 *
 * Astxx, the Asterisk C++ API and Utility Library.
 * Copyright (C) 2005-2007  Matthew A. Nicholson
 * Copyright (C) 2005-2007  Digium, Inc.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <map>
#include <astxx/agi.h>
#include <iostream>
#include <sstream>

using namespace std;
using namespace astxx;

int main(int argc, char** argv)
{
   cerr << "Welcome to the interactive AGI test.\n"
      "\n"
      "In this test you will pretend to be the AGI server.\n\n";

   cerr << "The agi AGI environment is composed of several values in the following format:\n"
      "\n"
      "   agi_var: var value\n"
      "   agi_another_var: another value\n"
      "\n"
      "The environment should be followed by new line.\n"
      "\n"
      "Enter the environment below:\n";

   try {
      astxx::agi& agi = astxx::agi::instance();

      cerr << "Environment is:\n";
      for (map<string, string>::const_iterator i = agi.begin();
            i != agi.end();
            i++) {
         cerr << i->first << ": " << i->second << "\n";
      }

      cerr << "I will now send several AGI commands.  Your response should be in the form of:\n"
         "\n"
         "200 result=1 (data)\n"
         "Please respond to the following commands:\n";

      agi.answer();

      astxx::agi& agi2 = astxx::agi::instance();
      stringstream ss;
      ss << "Channel status is " << agi2.channel_status();
      agi2.verbose(ss.str());

      ss.str("");

      ss << "Channel status is " << astxx::agi::instance().channel_status("Zap/1-1");
      agi2.verbose(ss.str() + "\n \n");
      cerr << "This concludes the interactive AGI test\n";
   }
   catch (astxx::agi::error &e) {
      stringstream ss;
      ss << "An error occured in the agi script:\n   " << e.what();
      cerr << ss.str() << endl;
      astxx::agi::instance().verbose(ss.str());
   }

   return 0;
}

