This file describes the coding style of the source code.  These guidelines
should be followed when submitting code to the project, modifing code, or when
ever else code might be written for this project.  Also contained in this file
is the perfered docbook documentation style.  (All files should contain vim 
modelines that follow this style)

Below is a sample code snippet.

#include <iostream>

int main(int argc, char** argv) {
   if (argc == 1) {
      cout << "Please pass a command line argument\n";
      return 1;
   } 
   else
      cout << "You passed enough command line arguments\n";

   for (int i = 1; i < 100; i++)
      cout << "Wasting time\n";

   return 0;
}

While reading this code you will notice several things about my coding style.  
First, all indents are three spaces.  NO TABS.  Next braces/curly brackets ({}) 
are placed on the same line as the code block they relate to preceeded by a 
space, including functions and namespaces.  All curly brackets should be at the 
same indentation level as the construct they are associated with.  Curly 
brackets should not be nested around if/else and try/catch chains.  All else 
and catch statements should appear on the next line after the previous closing 
curly bracket in the chain (NOT on the same line).

Labels such as case lables, 'public:', and the like are indented three spaces.  
The code they define should also be indented three spaces (think python).  Try 
to keep lines with in 80 characters, if you need to go past that in certain 
cases this is fine, but do not make it a habit.  Always keep comments with in 
79 characters (the 80th character may be a space if you wish).  More points to 
remember are listed below.

Binary operators should have spaces on both sides of them.  There should be no
space before and after () unless it makes the code eaiser to read.  There 
should be one space between constructs such as if, for, while, switch and the 
like, and their opening paren ('(').

(i + (j - h * k))
example_func("arg1", arg2)
if (i == 1) cout << "one";

The '*' in pointer declration and the '&' in reference declration should 
directly follow the type name.  The reasoning behind this is it is actually 
part of the type, not the address operator/dereference operator and not the 
multiplication or bitwise and operator.  Do not define multiple pointers on one 
line.

const int func_proto(int& ref_num, int* pt_num = 1);

const int func_proto(int& ref_num, int* pt_num) {
   int& another_ref;
   int* another_pointer;
   int just_an_int;
   return;
}

Local varaible and function names should be lowercase with underscores ('_').  
Global variable names should be uppercase with underscores.  Class and 
namespace names should be like function names.  Void in empty paramater lists 
is forbidden.

namespace super_namespace {
   class super_class {
      private:
         int super_private_member;

         int super_private_function();

      public:
         char super_public_function();
   };
}

Comments should be used frequently in code, but only for describing not readily
obvious code segments.  Single line (//) comments should be used as a rule of
thumb with multi line (/* ... */) comments reserved for large blocks.  All
functions should be documented with a doxygen style comment.  The @param javadoc
style should be used when arguments span a single line and the \param style
should be used in paragraphs.  Also try not to use things such as variable names
in your comments as this may change, and maintaining comments can become a
hassel.

/** Do super function stuff.
 *
 * This function takes your \a arguments and does some really cool super
 * function stuff with them.
 *
 * @param arg1 An integer argument.
 * @param arg2 A character argument.
 */
int function(int arg1, char arg2) {
   char dude[10] = "dude";
   char sweet[10] = "";

   // this obfucated peice of code copys one string into another
   while (*sweet++ = *dude++);

   return 1; // return one to the calling function /* <-- USELESS COMMENT */
}

Actual comments would need to actually tell what the function does and what the
arguments to it are for.  My obfucscated code should already be understood by
contributors to the project, but for lack of a better example it is used.
Notice I the comment for 'return 1;'.  This is un necessary and if the return
value was to change the comment would need to be updated to reflect this change.
describe it's use.  This sort of thing is self explainitory in the code and
should not be commented.  Brief non API functions may be commented using the
doxygen brief comment syntax of '///'.

vim: set tw=79 fo+=aw:
